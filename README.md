# Mock JSON API

Mocking a JSON API with GitLab pages. To call the "API":

```
# Via GitLab pages endpoint
curl -X GET https://briannqc.gitlab.io/mock-json-api/api/v1/products
```

